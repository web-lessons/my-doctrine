## Lecture 1

### links
- https://symfony.com/doc/current/doctrine.html
- https://symfony.com/doc/4.1/doctrine/lifecycle_callbacks.html
- https://www.doctrine-project.org/projects/doctrine-orm/en/latest/reference/annotations-reference.html

### steps

1. Profiler and debug:
- composer require profiler --dev
- composer require debug --dev
2. composer require orm
3. composer require maker --dev
4. php bin/console make:docker:database
5. symfony var:export --multiline (docker-compose.yaml)

Next:

  A) Run docker-compose up -d database to start your database container
     or docker-compose up -d to start all of them.
 
  B) If you are using the Symfony Binary, it will detect the new service automatically.
     Run symfony var:export --multiline to see the environment variables the binary is exposing.
     These will override any values you have in your .env files.
 
  C) Run docker-compose stop will stop all the containers in docker-compose.yaml.
     docker-compose down will stop and destroy the containers.

### Database structure workflow:

- php bin/console make:entity
- symfony console make:migration
- symfony console doctrine:migration:migrate

### CRUD
- symfony console make:crud

## Lecture 2

1. composer require --dev orm-fixtures
