<?php

namespace App\Repository;

use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    /**
     * @return Product[] Returns an array of Product objects
     */
    public function getProductsByPrice(int $min, int $max)
    {
        $queryBuilder = $this->createQueryBuilder('p');
        $expr = $queryBuilder->expr();

        $queryBuilder
            ->andWhere(
                $expr->andX(
                    $expr->gt('p.price', ':min'),
                    $expr->lt('p.price',':max'),

                )
            )
            ->setParameters([
                'min' => $min,
                'max' => $max,

            ])
            ->innerJoin('p.parameters', 'pr')
            ->orderBy('p.price', 'ASC')
            ->addOrderBy('p.name')
            ->setMaxResults(30)
        ;

        return $queryBuilder->getQuery()->getResult();
    }

    public function getOneByName(string $name): ?Product
    {
        $queryBuilder = $this->createQueryBuilder('p');
        $expr = $queryBuilder->expr();

        return $queryBuilder
            ->andWhere($expr->eq('p.name', ':name'))
            ->setParameter('name', $name)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
}
