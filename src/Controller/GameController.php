<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Feature;
use App\Entity\Parameter;
use App\Entity\Product;
use App\Repository\CategoryRepository;
use App\Repository\FeatureRepository;
use App\Repository\ParameterRepository;
use App\Repository\ProductRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/game')]
class GameController extends AbstractController
{
    #[Route('/parameter', name: 'parameter')]
    public function index(): Response
    {
        $entityManager = $this->getDoctrine()->getManager();

        /** @var ProductRepository $productRepository */
        $productRepository = $entityManager
            ->getRepository(Product::class);

        /** @var ParameterRepository $parameterRepository */
        $parameterRepository = $entityManager
            ->getRepository(Parameter::class);

        $product = $productRepository->find(2177);
        $paraters = $product->getParameters();
        foreach ($paraters as $parater) {
            dump($parater);
       }


        $param1 = $parameterRepository->find(1);
        $param2 = $parameterRepository->find(2);
        $param3 = $parameterRepository->find(3);



        $product->removeParameter($param1);
        $product->removeParameter($param2);
        $product->removeParameter($param3);

        $entityManager->persist($product);
        $entityManager->flush();

        dump($product);

        return new Response('OK');

    }

    #[Route('/feature', name: 'feature')]
    public function feature(): Response
    {
        $entityManager = $this->getDoctrine()->getManager();

        /** @var ProductRepository $productRepository */
        $productRepository = $entityManager
            ->getRepository(Product::class);

        /** @var FeatureRepository $featureRepository */
        $featureRepository = $entityManager
            ->getRepository(Feature::class);


        $feature1 = $featureRepository->find(1);
        $feature2 = $featureRepository->find(2);
        $feature3 = $featureRepository->find(3);

        $product = $productRepository->find(2177);

        $product->removeFeature($feature1);
        $product->removeFeature($feature2);
        $product->removeFeature($feature3);

        $entityManager->persist($product);
        $entityManager->flush();

        dd($product);

    }

    #[Route('/category', name: 'category')]
    public function category(): Response
    {
        $entityManager = $this->getDoctrine()->getManager();

        /** @var ProductRepository $productRepository */
        $productRepository = $entityManager
            ->getRepository(Product::class);

        $product = $productRepository->find(2177);

        $cat1 = new Category();
        $cat1->setName('cat1');

        $cat2 = new Category();
        $cat2->setName('cat2');

        $cat3 = new Category();
        $cat3->setName('cat3');

        $product->addCategory($cat1);
        $product->addCategory($cat2);
        $product->addCategory($cat3);

        $entityManager->persist($product);
        $entityManager->flush();

        dump($product);

        return new Response('OK');
    }

    #[Route('/remove', name: 'remove')]
    public function remove(): Response
    {
        $entityManager = $this->getDoctrine()->getManager();

        /** @var ProductRepository $productRepository */
        $productRepository = $entityManager
            ->getRepository(Product::class);

        /** @var CategoryRepository $categoryRepository */
        $categoryRepository = $entityManager
            ->getRepository(Category::class);

        $product = $productRepository->find(2177);

        $cat1 = $categoryRepository->find(1);
        $cat2 = $categoryRepository->find(2);
        $cat3 = $categoryRepository->find(3);

        $product->removeCategory($cat1);
        $product->removeCategory($cat2);
        $product->removeCategory($cat3);

        $entityManager->persist($product);
        $entityManager->flush();

        dump($product);

        return new Response('OK');
    }
}
