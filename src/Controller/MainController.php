<?php

namespace App\Controller;

use App\Entity\Product;
use App\Repository\ProductRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    #[Route('/main/create', name: 'create')]
    public function create(): Response
    {

        $product = new Product();
        $product->setName('product_name'.rand(1,100));
        $product->setDescription('ergergergerg');
        $product->setPrice(rand(1,100));
        $product->setIsActive(1);
//        $product->setCreatedAt(new \DateTime());


        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($product);
        $entityManager->flush();


        return new Response($product->getId());

    }

    #[Route('/main/update/{id}', name: 'update')]
    public function update(int $id): Response
    {
        $entityManager = $this->getDoctrine()->getManager();

        /** @var Product $product */
        $product = $entityManager
            ->getRepository(Product::class)
            ->find($id);
        $product->setDescription('updated_ergergergerg');
        $product->setPrice(rand(1,100));
        $product->setIsActive(0);
//        $product->setUpdatedAt(new \DateTime());

        $entityManager->persist($product);
        $entityManager->flush();


        return new Response('updated: '.$product->getId());

    }

    #[Route('/main/delete/{id}', name: 'delete')]
    public function delete(int $id): Response
    {
        $entityManager = $this->getDoctrine()->getManager();

        /** @var Product $product */
        $product = $entityManager
            ->getRepository(Product::class)
            ->find($id);
        $entityManager->remove($product);
        $entityManager->flush();

        dd($product);
    }


    #[Route('/main/find', name: 'find')]
    public function find(): Response
    {
        $entityManager = $this->getDoctrine()->getManager();

        /** @var ProductRepository $productRepository */
        $productRepository = $entityManager
            ->getRepository(Product::class);

        $products = $productRepository->findAll();

        dump($products);

        return new Response('OK!');
    }

    #[Route('/main/query', name: 'find')]
    public function query(): Response
    {
        $entityManager = $this->getDoctrine()->getManager();

        /** @var ProductRepository $productRepository */
        $productRepository = $entityManager
            ->getRepository(Product::class);

        $products = $productRepository->getProductsByPrice(35, 78);

        dd($products);

        return new Response('OK!');
    }
}
