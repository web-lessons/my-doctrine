<?php

namespace App\DataFixtures;

use App\Entity\Parameter;
use App\Entity\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class ParameterFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < 100; $i++) {
            $p = new Parameter();
            $p->setName('parameter '.$i);
            $manager->persist($p);
        }

        $manager->flush();
    }
}
